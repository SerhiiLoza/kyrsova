@extends('admin.layout')
@section('content')
    <div class="text-center">
    <h2>Список засобів</h2>
    </div>
    <div class="row justify-content-center">


    <table class="table table-sm" style="max-width: 1000px">
        <thead>
        <th cope="col">Назва</th>
        <th cope="col">Група</th>
        <th cope="col">Дія</th>
        </thead>
        @foreach ($automations as $automation)
            <tr>
                <td>
                    <a href="/automation/{{ $automation->name_id }}">{{ $automation->title}}</a>
                </td>
                <td>{{ $auto_groups[$automation->tech_id]}}</td>
                <td>
                    <a class="btn btn-warning" tabindex="-1" role="button" aria-disabled="true"  href="/admin/automation/{{ $automation->name_id }}/edit">Ред.</a>
                    <form style="float:right; padding: 0 15px;"
                          action="/admin/automation/{{ $automation->name_id }}"method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button class="btn btn-sm btn-danger btn-block">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    </div>
@endsection
