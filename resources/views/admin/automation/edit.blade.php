@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <h2>Редагування</h2>
    <form action="/admin/automation/{{ $automation->name_id}}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <label>Назва</label>
        <input type="text" name="title" value="{{$automation->title}}">
        <br/><br/>
        <label>Ціна</label>
        <input type="text" name="price" value="{{$automation->price}}">
        <br/><br/>
        <label>Група</label>
        <select name="group" >
            @foreach($auto_groups as $tech_id => $tech_title)
                <option value="{{ $tech_id }}">
                    {{ $tech_title }}
                </option>
            @endforeach
        </select >
        <br/><br/>
        <label>Виробник</label>
        <select name="manufacturer" >
            @foreach($auto_manuf as $company_id => $company_title)
                <option value="{{ $company_id }}">
                    {{ $company_title }}
                </option>
            @endforeach
        </select >
        <br/>
        <br/>
        <input type="submit" class="btn btn-success" value="Зберегти">
    </form>
@endsection
