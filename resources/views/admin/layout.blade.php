
<html>
<head>
    <title>Інформаційна система "Засоби автоматизації"</title>
    <link href="/css/app.css" rel="stylesheet">
</head>
<body class="bg-gray text-danger">
<h1>Адмін-панель</h1>
<div class="leftnav" style="float: left; width: 150px; height: 100%; margin-right:
30px;">
    <b>Перейти до</b>
    <ul>
        <li><a href="/admin/automation">Cписок</a></li>
        <li><a href="/admin/automation/create">Додати</a></li>
    </ul>
    <br/>
    <ul>
        <li><a href="/"> До головної</a></li>
    </ul>
</div>

    @yield('content')
<div class=" text-center align-items-center">
@include('layouts.footer')
</div>
</body>
</html>
