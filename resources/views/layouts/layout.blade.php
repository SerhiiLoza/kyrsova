<html>
<head>
    <title>Інформаційна система "Засоби автоматизації"</title>
    <link href="/css/app.css" rel="stylesheet">
</head>
<body class="bg-gray text-danger">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

@yield('page_title')
<br/><br/>
<div class="container">
    @yield('content')
</div>
<br/>
<br/>
<div class=" text-center align-items-center">
<a href="/admin/automation">Адмінка</a>
@include('layouts.footer')
</div>
</body>
</html>
