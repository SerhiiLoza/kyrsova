@extends('layouts.layout')
@section('page_title')
    <b>Інформація про {{ $automation->title}}</b>
@endsection
@section('content')
    <h4>Ви обрали {{ $auto_groups[$automation->tech_id] }} {{ $automation->title }} виробника {{ $automation->company}} за ціною  {{ $automation->price }}$  </h4>

    <a class="btn btn-success" href="/">Повернутися до списку</a>
@endsection
