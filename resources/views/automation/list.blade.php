@extends('layouts.layout')
@section('page_title')
    <div class=" text-center align-items-center">
        <h2>Список засобів автоматизації</h2>

        @endsection
        @section('content')

            <form method="get" action="/">
                <select name="group" >
                    <option value="0">всі групи</option>
                    @foreach($auto_groups as $tech_id => $tech_title)
                        <option value="{{ $tech_id }}"
                            {{ ( $tech_id == $tech_selected ) ? 'selected' : '' }}>
                            {{ $tech_title }}
                        </option>
                    @endforeach
                </select >
                <select name="manufacturer" >
                    <option value="0">всі компанії</option>
                    @foreach($auto_manuf as $company_id => $company_title)
                        <option value="{{ $company_id }}"
                            {{ ( $company_id == $company_selected ) ? 'selected' : '' }}>
                            {{ $company_title }}
                        </option>
                    @endforeach
                </select >
                <input type="submit" class="btn btn-success" value="Знайти" />
                <a href="/" class="btn btn-light" >Cкинути</a>
            </form>
            <div class="row justify-content-center">
        <table style="max-width: 60%">
            <th cope="col">Назва</th>
            <th cope="col">Ціна</th>
            <th cope="col">Група</th>
            @foreach ($automations as $automation)
                <tr>
                    <td>
                        <a  href="/automation/{{ $automation->name_id}}">{{ $automation->title}}</a>
                    </td>
                    <td>{{ $automation->price}}$</td>
                    <td>{{ $automation->group}}</td>
                </tr>
            @endforeach
        </table>
            </div> </div>
@endsection
