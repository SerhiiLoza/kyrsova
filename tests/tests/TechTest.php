<?php
use App\Model\Tech_zasoby;
class TechTest extends \PHPUnit\Framework\TestCase
{
	public function testPrice(){
		$stub=$this
			->getMockBuilder(Tech_zasoby::class)
			->disableOriginalConstructor()
			->getMock();
		$stub->method('getPrice')
			->willReturn('100');
		$this->assertSame('100', $stub->getPrice());
	}

}