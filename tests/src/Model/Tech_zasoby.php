<?php


namespace App\Model;


class Tech_zasoby
{
	private $group;
	private $name;
	private $company;
	private $price;

	/**
	 * Tech_zasoby constructor.
	 * @param $group
	 * @param $name
	 * @param $company
	 * @param $price
	 */
	public function __construct($group, $name, $company, $price)
	{
		$this->group = $group;
		$this->name = $name;
		$this->company = $company;
		$this->price = $price;
	}

	/**
	 * @return mixed
	 */
	public function getGroup()
	{
		return $this->group;
	}

	/**
	 * @param mixed $group
	 */
	public function setGroup($group): void
	{
		$this->group = $group;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name): void
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getCompany()
	{
		return $this->company;
	}

	/**
	 * @param mixed $company
	 */
	public function setCompany($company): void
	{
		$this->company = $company;
	}

	/**
	 * @return mixed
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * @param mixed $price
	 */
	public function setPrice($price): void
	{
		$this->price = $price;
	}

}