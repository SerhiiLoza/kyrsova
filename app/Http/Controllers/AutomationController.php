<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Automation;
use Illuminate\Support\Facades\Redirect;
class AutomationController extends Controller
{
    public function index(Request $request){
        $tech = $request->input('group', null);
        $manufacturer = $request->input('manufacturer', null);
        $company = $request->input('company', null);
        $model_automations= new Automation();
        $automations= $model_automations->getTitle($tech,$manufacturer);
        return view('automation.list', [
                'automations' => $automations,
                'auto_groups' => Automation::$auto_groups,
                'auto_manuf' => Automation::$auto_manuf,
                'tech_selected' => $tech,
                'company_selected' => $manufacturer,
                'company' => $company]
        );
    }
    public function automation($id){
        $model_automations = new Automation();
        $automation = $model_automations->getAutomationByID($id);
        return view('automation.automation',['auto_groups' => Automation::$auto_groups])->with('automation', $automation );
    }
}
