<?php

namespace App\Http\Controllers;
use App\Models\Automation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\AutomationController;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $automations= Automation::get();
        return view('admin.automation.list', ['automations' => $automations,'auto_groups' => Automation::$auto_groups,'auto_manuf' => Automation::$auto_manuf]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.automation.add', ['auto_groups' => Automation::$auto_groups,'auto_manuf' => Automation::$auto_manuf]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = $request->input('title');
        $tech = $request->input('group');
        $price= $request->input('price');
        $manufacturer= $request->input('manufacturer');
        $automation = new Automation();
        $automation->title = $title;
        $automation->company_id = $manufacturer;
        $automation->tech_id = $tech;
        $automation->price = $price;
        $automation->save();
        return Redirect::to('/admin/automation');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $automation = Automation::where('name_id', $id)->first();
        return view('admin.automation.edit', [
            'automation' => $automation,'auto_groups' => Automation::$auto_groups,'auto_manuf' => Automation::$auto_manuf
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $automation = Automation::where('name_id', $id)->first();
        $automation->title = $request->input('title');
        $automation->tech_id = $request->input('group');
        $automation->company_id = $request->input('manufacturer');
        $automation->price = $request->input('price');
        $automation->save();
        return Redirect::to('/admin/automation');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Automation::destroy($id);
        return Redirect::to('/admin/automation');
    }
}
