<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Automation extends Model
{
    use HasFactory;
    protected $primaryKey = 'name_id';
    public $timestamps = false;

    public static $auto_groups = array(
        '1' => 'Датчик',
        '2' => 'Перетворювач',
        '3' => 'Контроллер',
    );
    public static $auto_manuf = array(
        '1' => 'Frecon',
        '2' => 'OWEN',
        '3' => 'Siemens',
    );

    public function getTitle($tech,$manufacturer)
    {
        $query = DB::table('automations','a');
        $query->select('*','group_automation.group' )
            ->join('group_automation','a.tech_id', '=','group_automation.tech_id')
            ->orderBy('title');
        if($tech){
            $query->where('a.tech_id', '=', $tech);
        }
        if($manufacturer){
            $query->where('a.company_id', '=', $manufacturer);
        }
        $automations = $query->get();
        return $automations ;
    }


    public function getAutomationByID($id){
        if(!$id) return null;
        $automations = DB::table('automations','a')
            ->select('*')
            ->join('group_automation','a.tech_id', '=','group_automation.tech_id')
            ->join('manufacturer','a.company_id', '=','manufacturer.company_id')
            ->where('a.name_id', $id)
            ->get()->first();
        return $automations;
    }
}
